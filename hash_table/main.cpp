// tabledemo.cpp - demonstration program for Table
// cmc, 4/4/2016

#include <iostream>
#include <fstream>
#include "table.h"
using namespace std;

unsigned int user_get(Table &t);
unsigned int user_remove(Table &t);

int main() {
    
    cout << "SECTION 1" << endl;
    Table a, b(5);
    
    std::ifstream input;
    input.open("f.txt");
    user_get(a);
    
    if (!input.good()) {
        cout << "No fips.txt in current directory. Quitting\n";
        return 1;
    }
    Table t3(256, input);
    Table copy(t3);

    copy.remove(51069);
    copy.remove(51069);
    cout << copy.get(51069);
    copy.remove(51);
    copy.remove(51);
      copy.remove(51);
    copy.remove(49029);
    
    copy.remove(51053);
    
  
    copy.remove(51069);
    
    copy.remove(47031);
    
    
    copy.remove(34003);
    
    copy.remove(27151);
    
    copy.remove(16053);
    
    
    copy.remove(13275);
    copy.remove(51069);
    
    copy.remove(47031);
    
    
    copy.remove(34003);
    
    copy.remove(27151);
    
    copy.remove(16053);
    
    
    copy.remove(13275);
    cout << copy << endl ;
    
    cout << "END OF COPY" << endl ;
    
    t3.put(51069, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    t3.put(49029, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    t3.put(51053, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    t3.put(51069, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    t3.put(47031, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    
    t3.put(34003, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    t3.put(27151, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    t3.put(16053, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    
    t3.put(13275, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
 
    
    t3.remove(51053);
    
    cout << "ORIGINAL <<<<<<< " << endl ;
    cout << t3 << endl ;
   
    
    
    
    
    
    cout << "a: " << endl  << a  << endl ;
    cout << "b: " << endl << b << endl ;

    cout << "SECTION 2" << endl ;
    
    Table c = b, d(c) ;
    cout << "c: " << endl << c << endl ;
    cout << "d: " << endl << d << endl ;
    
    cout << "SECTION 3" << endl ;
    a = a ;
    a = b ;
    cout << "a: " << endl << a << endl ;
    cout << "b: " << endl << b << endl ;
    
    cout << "Section 4" << endl;
    cout << "sum of elements in a: " << a.getUsed() << endl;
    
    
    cout << "**DONE**" << endl;
    
    return 0;

//    cout << "\nFinally demonstrate larger table\n";
//    std::ifstream input;
//    input.open("f.txt");
//    
//
//    
//    if (!input.good()) {
//        cout << "No fips.txt in current directory. Quitting\n";
//        return 1;
//    }
//    Table t3(3142, input);
//    cout << "Try getting some entries by FIPS code keys\n"
//    << "(enter 0 key to quit)\n";
//    while (user_get(t3) != 0)
//        ;
//    cout << "Print large table to sortedfips.txt?\n";
//    char ans;
//    cin >> ans;
//    if (ans == 'Y' || ans == 'y') {
//        unsigned int start = Entry::access_count();
//        ofstream out;
//        out.open("sortedflips.txt");
//        out << t3;
//        out.close();
//        cout << "done writing to sortedfips.txt ... required "
//        << Entry::access_count() - start << " accesses\n";
//    }
    return 0;
}

unsigned int user_get(Table &t) {
    unsigned int key;
    cout << "Enter key to get:\n";
    cin >> key;
    if (key != 0 ) {
        unsigned int start = Entry::access_count();
        cout << "data at key " << key << ": " << t.get(key) << endl;
        cout << "(accesses: " <<
        Entry::access_count() - start << ")\n";
    }
    return key;
}

unsigned int user_remove(Table &t) {
    unsigned int key;
    cout << "Enter key to remove:\n";
    cin >> key;
    if (key != 0) {
        unsigned int start = Entry::access_count();
        if (t.remove(key))
            cout << "removed key: " << key << endl;
        else
            cout << "did not find key: " << key << endl;
        cout << "(accesses: "
        << Entry::access_count() - start << ")\n";
    }
    return key;
}

