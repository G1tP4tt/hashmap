//
//  table.cpp
//  hash_table
//
//  Created by Patrice Bender on 14.04.16.
//  Copyright © 2016 Patrice Bender. All rights reserved.
//

#include "table.h"
#include <iostream>
#include <algorithm>
#include <assert.h>

using namespace std;


Table::Table(unsigned int max_entries):used(0),capacity((max_entries *4) + 3),max(max_entries){
    
    data_t = new Entry[capacity] ;
    
};


Table::Table(unsigned int max_entries, std::istream &inp):used(0),capacity((max_entries *4) + 3),max(max_entries){
    
    int key ;
    string data;
    data_t = new Entry[capacity] ;
    
    
    while (used < max_entries) {
        inp >> key;
        // get data in two parts to handle white space
        string first_word, rest_of_line;
        inp >> first_word;
        getline(inp, rest_of_line);
        data = first_word + rest_of_line;
        
        put(key, data) ;
    }
};
Table::Table(const Table &other) {
    capacity = other.capacity;
    data_t = new Entry[capacity];
    for (int i=0; i<capacity; i++)
        data_t[i] = other.data_t[i];
    this->used = other.used ;
    this->max = other.max ;
    //    cout << "copy ctor: object @ " << this << ", data[" << capacity
    //    << "] @ " << data_t << endl;
}

Table::~Table(){
    
    
    delete [] data_t ;
    //    cout << "dtor: object @ " << this << ", data[" << capacity
    //    << "] @ " << data_t << endl;
};

Table& Table::operator=(const Table &right){
    
    //cout << "assignment op: object @ " << this;
    if (this != &right) {
        if (capacity != right.capacity) {
            //cout << ", deleting data[" << capacity << "] @ " << data_t;
            delete [] data_t;
            capacity = right.capacity;
            data_t = new Entry[capacity];
        }
        for (int i=0; i<capacity; i++)
            data_t[i] = right.data_t[i];
        this->used = right.used ;
        this->max = right.max ;
        //        cout << ", copied data[" << capacity << "] to " << data_t << endl;
    }
    
    
    
    
    return *this;
    
    
};


void Table::put(const Entry e)
{
    bool already_present ;
    size_t index ;
    int key = e.get_key() ;
    
    if (data_t[hash(key)].get_data() == "" ) {
        
        index = hash(key) ;
        
    }else{
        searchIndex(key, already_present, index) ;
        
        
        if (already_present) {
            --used ;
        }
    }
    
    ++used ;
    data_t[index] = e ;
    
} ;

void Table::put(unsigned int key, std::string data){
    
    Entry temp;
    size_t index ;
    bool found ;
    
    temp.set_key(key);
    temp.set_data(data);
    
    
    
    if (data_t[hash(key)].get_data() == "" || data_t[hash(key)].get_data() == "REMOVED" ) {
        
        index = hash(key) ;
        
        
    }else{
        searchIndex(key, found, index) ;
        
        if (found) {
            --used ;
        }
    }
    if (!found && used == max) {
        
    }else{
        ++used ;
        data_t[index] = temp ;
    }

    
    
};

int Table::getUsed()const{
    return used ;
}

void Table::searchIndex(int key,bool& found, size_t& i) const{
    size_t count = 0  ; //Number of searched entries
    
    i = hash(key) ;
    
    if (data_t[i] == key) {
        found = true ;
    }else{
        while (count < capacity && (data_t[i] != 0 && data_t[i] != key )) {
            
            ++count ;
            i = nextIndex(i) ;
            if (data_t[i] == key) {
                found = true ;
                break;
            }
            
        }
        
    }
    
    
    
    
};




std::string Table::get(unsigned int key) const{
    
    size_t index ;
    int code = hash(key) ;
    bool found ;
    
    if (data_t[code].get_data() == "" || data_t[code].get_data() == "REMOVED" ) {
        return "" ;
    }
    
    searchIndex2(key,  index);
    
    if (data_t[index].get_data() == "" || data_t[index].get_data() == "REMOVED" ) {
        return "" ;
    }
    
    
    
    return(this->data_t[index].get_data());
    
    
    
    
};


int Table::hash(unsigned int key) const{
    
    
    //cout << "Key = " << key << " HASH: " << (key % capacity) << endl;
    
    return (key*611953) % capacity ;
};


void Table::searchIndex2(int key, size_t& i) const{
    size_t count = 0  ; //Number of searched entries
    
    i = hash(key) ;
    
    while ( data_t[i].get_data() != "" && (data_t[i].get_data() != "REMOVED")) {
        
        if(data_t[i].get_key() == key){
            
            return;}
        
        i = nextIndex(i) ;
        ++count ;
        
    }
    
    
    
    
};

int Table::nextIndex(int i) const{
    return (i + 73673) % capacity;
};



bool Table::remove(unsigned int key){
    bool found;
    size_t index  = hash(key);
    
    
    if (data_t[hash(key)].get_data() == "" ) {
        
        return false;
        
    }
    
    while (data_t[index].get_key() != key) {
        if (data_t[index].get_data() == "") {
            return false ;
        }
        index = nextIndex(index) ;
    }
    if (data_t[index].get_data() == "REMOVED") {
        return false ;
    }
    
    data_t[index].set_key(0)  ;
    data_t[index].set_data("REMOVED");
    used-- ;
    return true ;
};

// partitions portion of integer array for quicksort
void partition(int a[], int &i, int &j) {
    int pivot = a[(i + j) / 2]; // middle element is the pivot
    
    do {
        // scan i from left, then j from right
        while (a[i] < pivot) i++;   // note:
        while (a[j] > pivot) j--;   // i and j are reference parameters
        
        // swap if pointers haven't crossed yet
        if (i <= j) {
            swap(a[i], a[j]);
            i++;
            j--;
        }
    }while(i <= j);  // keep scanning until pointers cross
}

// recursive quick sort for integer array
void quicksort(int a[], int left, int right) {
    int i, j;
    
    
    if (left < right) {
        i = left; j = right;
        partition(a, i, j);
        quicksort(a, left, j);
        quicksort(a, i, right);
    }
    
    
}

std::ostream& operator<< (std::ostream &out, const Table &t){
    
    int how_many =t.used ;
    int temp[how_many-1] ;
    
    int count = 0;
    int i=0 ;
    
    
    while (count < how_many){
        
        if (t.data_t[i] != 0 && t.data_t[i].get_data() != "REMOVED") {
            
            temp[count] = t.data_t[i] ;
            ++count ;
            
        }
        i++ ;
    }
    
    if(how_many == 0) return out ;
    ::quicksort(temp, 0 , how_many -1);
    
    
    
    
    
    size_t next = 0 ;
    i = 0 ;
    int  o = 0;
    for (; i < t.used; i++) {
        
        o= temp[i] ;
        
        
        next = t.hash(o) ;
        bool test = (t.data_t[next] == o) ;
        
        if (test) {
            
            out << t.data_t[next] << endl;
        }else{
            t.searchIndex2(o, next);
            if (t.data_t[next].get_data() == "REMOVED") {
                continue;
            }else{
                out << t.data_t[next] << endl;
                
            }
        }
        
    }
    
    
    
    
    
    
    
    
    return out ;
};
