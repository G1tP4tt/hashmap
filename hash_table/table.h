#ifndef table_h
#define table_h
#include <stdio.h>
#include "entry.h"

class Table{
    
public:
    
    
    //defines Entry Pointer
    typedef Entry* entryPtr ;
    
    //constructor
    
    Table(unsigned int max_entries = 100);
    Table(unsigned int entries, std::istream &input);
    ~Table() ;
    Table(const Table &other);
    Table& operator= (const Table &right);
    
    int getUsed() const;
    void print(Table &t) ;
    void put(unsigned int key, std::string data);
    void put(const Entry e);
    //The first of these functions creates a new Entry to put in the Table. The second one puts a copy of the parameter in the Table. In cases where the Table already contains an Entry with the given key, these functions act to update the Entry for that key. The Table is not allowed to contain duplicate keys.
    
    std::string get(unsigned int key) const ;//This function returns an empty string if the Table has no Entry with the given key.
    bool remove(unsigned int key); //This function returns true if it removes an Entry, or false if the Table has no such Entry.
    
    friend std::ostream& operator<< (std::ostream &out, const Table &t); //This function is expected to print each Entry in the Table to a separate line of the given output stream in the order of their key values.
    
    void partition(int data[ ], size_t n, size_t& pivot_index) const;
    void quicksort(int data[ ]) const;
    
    
private:
    
    //Member variables
    int capacity;
    int used = 0;
    int max ;
    
    //Hash array
    Entry *data_t ;
    
    
    //Helper functions
    int hash(unsigned int key) const;
    
    void searchIndex(int key, bool& found,size_t& i) const;
    void searchIndex2(int key,size_t& i) const;
    
    int nextIndex(int i) const;
    
    
    
    
    
};


#endif /* table_h */